/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest;

import com.selfxdsd.api.Self;
import com.selfxdsd.api.User;
import com.selfxdsd.rest.exception.ForbiddenAccessException;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;

/**
 * Unit tests for {@link UserProducer}.
 * @author Mihai Andronache (amihaiemil@gmail.com)
 * @version $Id$
 * @since 0.0.1
 */
public final class UserProducerTestCase {

    /**
     * An exception is thrown if the Authorization HTTP Header is missing.
     */
    @Test(expected = ForbiddenAccessException.class)
    public void exceptionWhenAuthorizationIsMissing() {
        new UserProducer(
            Mockito.mock(Self.class)
        ).authenticate(Mockito.mock(HttpServletRequest.class));
    }

    /**
     * An exception is thrown if the Authorization HTTP Header is missing the
     * Bearer or Token prefix.
     */
    @Test(expected = ForbiddenAccessException.class)
    public void exceptionWhenAuthorizationMissingBearer() {
        final HttpServletRequest request = Mockito.mock(
            HttpServletRequest.class
        );
        Mockito.when(request.getHeader("Authorization")).thenReturn(
            "tokenAuth1234"
        );
        new UserProducer(
            Mockito.mock(Self.class)
        ).authenticate(request);
    }

    /**
     * An exception is thrown if the Authorization HTTP Header is not correct.
     */
    @Test(expected = ForbiddenAccessException.class)
    public void exceptionWhenAuthorizationNotCorrect() {
        final HttpServletRequest request = Mockito.mock(
            HttpServletRequest.class
        );
        Mockito.when(request.getHeader("Authorization")).thenReturn(
            "wrong tokenAuth1234"
        );
        new UserProducer(
            Mockito.mock(Self.class)
        ).authenticate(request);
    }

    /**
     * An exception is thrown if User authentication fails.
     */
    @Test(expected = ForbiddenAccessException.class)
    public void exceptionWhenAuthFailed() {
        final HttpServletRequest request = Mockito.mock(
                HttpServletRequest.class
        );
        Mockito.when(request.getHeader("Authorization")).thenReturn(
                "Bearer tokenAuth1234"
        );
        final Self core = Mockito.mock(Self.class);
        Mockito.when(core.authenticate("tokenAuth1234"))
                .thenReturn(null);

        new UserProducer(
                Mockito.mock(Self.class)
        ).authenticate(request);
    }

    /**
     * Authentication in the core works with a Token Authorization.
     */
    @Test
    public void authenticationWorksToken() {
        final HttpServletRequest request = Mockito.mock(
            HttpServletRequest.class
        );
        Mockito.when(request.getHeader("Authorization")).thenReturn(
            "Token tokenAuth1234"
        );
        final User authenticated = Mockito.mock(User.class);
        final Self core = Mockito.mock(Self.class);
        Mockito.when(core.authenticate("tokenAuth1234"))
            .thenReturn(authenticated);

        MatcherAssert.assertThat(
            new UserProducer(core).authenticate(request),
            Matchers.is(authenticated)
        );
    }

    /**
     * Authentication in the core works with a Bearer Authorization.
     */
    @Test
    public void authenticationWorksBearer() {
        final HttpServletRequest request = Mockito.mock(
            HttpServletRequest.class
        );
        Mockito.when(request.getHeader("Authorization")).thenReturn(
            "Bearer tokenAuth1234"
        );
        final User authenticated = Mockito.mock(User.class);
        final Self core = Mockito.mock(Self.class);
        Mockito.when(core.authenticate("tokenAuth1234"))
            .thenReturn(authenticated);

        MatcherAssert.assertThat(
            new UserProducer(core).authenticate(request),
            Matchers.is(authenticated)
        );
    }

}
