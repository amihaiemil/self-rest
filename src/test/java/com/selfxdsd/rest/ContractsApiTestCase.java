/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest;

import com.selfxdsd.api.Contract;
import com.selfxdsd.api.Contributor;
import com.selfxdsd.api.User;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.json.Json;
import java.io.StringReader;
import java.math.BigDecimal;

/**
 * Unit tests for {@link ContractsApi}.
 * @author Mihai Andronache (amihaiemil@gmail.com)
 * @version $Id$
 * @since 0.0.1
 */
public final class ContractsApiTestCase {

    /**
     * The GET Contract endpoint returns 204 NO CONTENT if
     * the authenticated User is not a Contributor.
     */
    @Test
    public void fetchContractNoContributor() {
        final User user = Mockito.mock(User.class);
        Mockito.when(user.asContributor()).thenReturn(null);

        MatcherAssert.assertThat(
            new ContractsApi(user).contract(
                "mihai", "test", "DEV"
            ).getStatusCode(),
            Matchers.equalTo(HttpStatus.NO_CONTENT)
        );
    }

    /**
     * The GET Contract endpoint returns 204 NO CONTENT if the
     * Contributor doesn't have the specified Contract.
     */
    @Test
    public void fetchContractNotFound() {
        final Contributor contributor = Mockito.mock(Contributor.class);
        Mockito.when(contributor.provider()).thenReturn("github");
        Mockito.when(
            contributor.contract("mihai/test", "github", "DEV")
        ).thenReturn(null);
        final User user = Mockito.mock(User.class);
        Mockito.when(user.asContributor()).thenReturn(contributor);

        MatcherAssert.assertThat(
            new ContractsApi(user).contract(
                "mihai", "test", "DEV"
            ).getStatusCode(),
            Matchers.equalTo(HttpStatus.NO_CONTENT)
        );
    }

    /**
     * The GET Contract endpoint returns the found Contract with status
     * 200 OK.
     */
    @Test
    public void fetchContract() {
        final Contract contract = Mockito.mock(Contract.class);
        final Contract.Id contractId = new Contract.Id(
            "mihai/test",
            "amihaiemil",
            "github",
            "DEV"
        );
        Mockito.when(contract.contractId()).thenReturn(contractId);
        Mockito.when(contract.hourlyRate())
            .thenReturn(BigDecimal.valueOf(1500));

        final Contributor contributor = Mockito.mock(Contributor.class);
        Mockito.when(contributor.username()).thenReturn("amihaiemil");
        Mockito.when(contributor.provider()).thenReturn("github");
        Mockito.when(
            contributor.contract("mihai/test", "github", "DEV")
        ).thenReturn(contract);

        final User user = Mockito.mock(User.class);
        Mockito.when(user.asContributor()).thenReturn(contributor);

        final ResponseEntity<String> resp = new ContractsApi(user).contract(
            "mihai", "test", "DEV"
        );
        MatcherAssert.assertThat(
            resp.getStatusCode(),
            Matchers.equalTo(HttpStatus.OK)
        );
        MatcherAssert.assertThat(
            Json.createReader(
                new StringReader(resp.getBody())
            ).readObject(),
            Matchers.equalTo(
                Json.createObjectBuilder()
                    .add("repoFullName", "mihai/test")
                    .add("provider", "github")
                    .add("username", "amihaiemil")
                    .add("role", "DEV")
                    .add("hourlyRate", 1500)
                    .add("markedForRemoval", "null")
                    .build()
            )
        );
    }

}
