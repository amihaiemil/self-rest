/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest.output;

import com.selfxdsd.api.Project;
import com.selfxdsd.api.Provider;
import com.selfxdsd.api.User;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;


/**
 * Unit tests for {@link JsonProject}.
 * @author criske
 */
public final class JsonProjectTestCase {
    

    /**
     * JsonProject can be created from Project.
     */
    @Test
    public void buildsJsonFromProject() {
        final Project project = Mockito.mock(Project.class);
        final User owner = Mockito.mock(User.class);

        Mockito.when(project.repoFullName()).thenReturn("mihai/test");
        Mockito.when(project.provider()).thenReturn(Provider.Names.GITHUB);
        Mockito.when(project.owner()).thenReturn(owner);
        Mockito.when(owner.username()).thenReturn("mihai");
        Mockito.when(owner.email()).thenReturn("amihaiemil@gmail.com");


        final JsonObject jsonProject = new JsonProject(project);

        MatcherAssert.assertThat(
            jsonProject.getString("repoFullName"),
            Matchers.equalTo("mihai/test")
        );
        MatcherAssert.assertThat(
            jsonProject.getString("name"),
            Matchers.equalTo("test")
        );
        MatcherAssert.assertThat(
            jsonProject.getString("provider"),
            Matchers.equalTo(Provider.Names.GITHUB)
        );
        MatcherAssert.assertThat(
            jsonProject.getJsonObject("owner"),
            Matchers.equalTo(Json.createObjectBuilder()
                .add("username", "mihai")
                .add("email", "amihaiemil@gmail.com")
                .add("provider", Provider.Names.GITHUB)
                .build())
        );
        MatcherAssert.assertThat(
            jsonProject.getJsonArray("links"),
            Matchers.equalTo(JsonValue.EMPTY_JSON_ARRAY)
        );
    }

}
