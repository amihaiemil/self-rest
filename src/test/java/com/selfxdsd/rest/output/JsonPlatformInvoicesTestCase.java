/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest.output;

import com.selfxdsd.api.PlatformInvoice;
import com.selfxdsd.api.PlatformInvoices;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.json.Json;
import javax.json.JsonArray;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Unit tests for {@link JsonPlatformInvoices}.
 * @author Mihai Andronache (amihaiemil@gmail.com)
 * @version $Id$
 * @since 0.0.1
 * @checkstyle LineLength (1000 lines)
 * @checkstyle ExecutableStatementCount (1000 lines)
 */
public final class JsonPlatformInvoicesTestCase {

    /**
     * It can represent the PlatformInvoices as JsonArray.
     */
    @Test
    public void representsJsonArray() {
        final PlatformInvoice invoiceOne = Mockito.mock(PlatformInvoice.class);
        Mockito.when(invoiceOne.id()).thenReturn(1);
        Mockito.when(invoiceOne.serialNumber()).thenReturn("SLFX0000001");
        Mockito.when(invoiceOne.createdAt()).thenReturn(
            LocalDateTime.of(2021, 2, 8, 0, 0, 0)
        );
        Mockito.when(invoiceOne.billedBy()).thenReturn("XD Tech S.R.L.");
        Mockito.when(invoiceOne.billedTo()).thenReturn("Mihai Andronache");
        Mockito.when(invoiceOne.commission()).thenReturn(BigDecimal.valueOf(100));
        Mockito.when(invoiceOne.vat()).thenReturn(BigDecimal.valueOf(19));
        Mockito.when(invoiceOne.totalAmount()).thenReturn(BigDecimal.valueOf(119));
        Mockito.when(invoiceOne.transactionId()).thenReturn("transaction123");
        Mockito.when(invoiceOne.paymentTime()).thenReturn(
            LocalDateTime.of(2021, 2, 8, 0, 0, 0)
        );

        final PlatformInvoice invoiceTwo = Mockito.mock(PlatformInvoice.class);
        Mockito.when(invoiceTwo.id()).thenReturn(2);
        Mockito.when(invoiceTwo.serialNumber()).thenReturn("SLFX0000001");
        Mockito.when(invoiceTwo.createdAt()).thenReturn(
            LocalDateTime.of(2021, 2, 8, 0, 0, 0)
        );
        Mockito.when(invoiceTwo.billedBy()).thenReturn("XD Tech S.R.L.");
        Mockito.when(invoiceTwo.billedTo()).thenReturn("Mihai Andronache");
        Mockito.when(invoiceTwo.commission()).thenReturn(BigDecimal.valueOf(100));
        Mockito.when(invoiceTwo.vat()).thenReturn(BigDecimal.valueOf(19));
        Mockito.when(invoiceTwo.totalAmount()).thenReturn(BigDecimal.valueOf(119));
        Mockito.when(invoiceTwo.transactionId()).thenReturn("transaction123");
        Mockito.when(invoiceTwo.paymentTime()).thenReturn(
            LocalDateTime.of(2021, 2, 8, 0, 0, 0)
        );

        final PlatformInvoices invoices = Mockito.mock(PlatformInvoices.class);
        Mockito.when(invoices.spliterator()).thenReturn(
            List.of(invoiceOne, invoiceTwo).spliterator()
        );

        final JsonArray array = new JsonPlatformInvoices(invoices);
        MatcherAssert.assertThat(
            array.size(),
            Matchers.equalTo(2)
        );
        MatcherAssert.assertThat(
            array.getJsonObject(0),
            Matchers.equalTo(
                Json.createObjectBuilder()
                    .add("id", 1)
                    .add("serialNumber", "SLFX0000001")
                    .add("createdAt", "2021-02-08T00:00")
                    .add("billedBy", "XD Tech S.R.L.")
                    .add("billedTo", "Mihai Andronache")
                    .add("commission", 100)
                    .add("vat", 19)
                    .add("totalAmount", 119)
                    .add("transactionId", "transaction123")
                    .add("paidAt", "2021-02-08T00:00")
                    .build()
            )
        );
        MatcherAssert.assertThat(
            array.getJsonObject(1),
            Matchers.equalTo(
                Json.createObjectBuilder()
                    .add("id", 2)
                    .add("serialNumber", "SLFX0000001")
                    .add("createdAt", "2021-02-08T00:00")
                    .add("billedBy", "XD Tech S.R.L.")
                    .add("billedTo", "Mihai Andronache")
                    .add("commission", 100)
                    .add("vat", 19)
                    .add("totalAmount", 119)
                    .add("transactionId", "transaction123")
                    .add("paidAt", "2021-02-08T00:00")
                    .build()
            )
        );
    }

    /**
     * It can be an empty JsonArray.
     */
    @Test
    public void representsEmptyArray() {
        final PlatformInvoices invoices = Mockito.mock(PlatformInvoices.class);
        Mockito.when(invoices.spliterator()).thenReturn(
            new ArrayList<PlatformInvoice>().spliterator()
        );

        final JsonArray array = new JsonPlatformInvoices(invoices);
        MatcherAssert.assertThat(
            array.size(),
            Matchers.equalTo(0)
        );
    }

}
