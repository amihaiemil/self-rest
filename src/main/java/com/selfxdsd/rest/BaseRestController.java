/**
 * Copyright (c) 2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest;

import com.selfxdsd.rest.exception.ForbiddenAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Base REST controller for all controllers to inherit. Contains Exception
 * Handlers.
 * @author Nikita Monokov (nmonokov@gmail.com)
 * @version $Id$
 * @since 0.0.1
 * @checkstyle ReturnCount (100 lines)
 * @todo #12:60min Implement the ContributorInvoicesApi which should represent
 *  the invoices emitted by the authenticated User, in a certain Contract.
 *  It should continue the path of ContractsApi (base path
 *  /contracts/{contractId} /invoices). We can start with an endpoint for
 *  fetching a single Invoice.
 */
public class BaseRestController {

    /**
     * Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(
        BaseRestController.class
    );

    /**
     * Return any {@link BeanCreationException} as FORBIDDEN, if the root
     * cause is {@link ForbiddenAccessException}.
     * @param exception A {@link BeanCreationException}.
     * @return Exception's message in JSON format.
     */
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(BeanCreationException.class)
    public String handleBeanCreationException(
        final BeanCreationException exception
    ) {
        LOG.warn(
            "BeanCreationException occurred: " + exception.getMessage()
        );
        final Throwable cause = exception.getCause().getCause();
        if(cause instanceof ForbiddenAccessException) {
            LOG.warn("ForbiddenAccessException occurred!");
            return cause.getMessage();
        } else {
            return "Something went wrong on our side.";
        }
    }

    /**
     * Return any {@link IllegalStateException} as SERVER ERROR.
     * @param exception A {@link IllegalStateException}.
     * @return Exception's message in JSON format.
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(IllegalStateException.class)
    public String handleIllegalStateException(
        final IllegalStateException exception
    ) {
        LOG.error(
            "IllegalStateException occurred: ", exception
        );
        return "Something went wrong on our side.";
    }

    /**
     * Return any {@link NullPointerException} as SERVER ERROR.
     * @param exception A {@link NullPointerException}.
     * @return Exception's message in JSON format.
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(NullPointerException.class)
    public String handleNpe(
        final NullPointerException exception
    ) {
        LOG.error(
            "NullPointerException occurred: ", exception
        );
        return "Something went wrong on our side.";
    }

}
